package com.pizza.combomenu.api;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CComboMenu {

	@CrossOrigin
	@GetMapping("")
	public ArrayList<CMenu> getComboPizzaArray() {
		ArrayList<CMenu> comboMenu = new ArrayList<CMenu>();
		CMenu comboS = new CMenu("S", 20, 2, 200, 2, 150000);
		CMenu comboM = new CMenu("M", 25, 4, 300, 3, 200000);
		CMenu comboL = new CMenu("L", 30, 8, 500, 4, 250000);
		comboMenu.add(comboS);
		comboMenu.add(comboM);
		comboMenu.add(comboL);
		return comboMenu;
	}
}
